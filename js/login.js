// API URLs
const baseURL = "http://localhost:8090";
const loginEndpoint =  baseURL + "/usuarios/login";

// DOM
const btnRegister = document.querySelector("#register");
const btnLogin = document.querySelector("#login");
const txtUsername = document.querySelector("#username");
const txtPassword = document.querySelector("#password");

// Events
function checkLoggedInEvent() {
    let token = localStorage.getItem("token");
    if (token !== null) {
        window.location = "portal.html";
    }
}

function registerEvent() {
    window.location = "register.html";
}

function loginEvent() {
    event.preventDefault();
    
    let data = {
        "username": txtUsername.value,
        "senha": txtPassword.value
    }
    
    $.ajax({
        url: loginEndpoint,
        type: "post",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        data: JSON.stringify(data),
        dataType: "json"
      })
      .done((data, textStatus, request) => {
        let authorizationHeader = request.getResponseHeader("Authorization");
        if (authorizationHeader !== null) {            
            localStorage.setItem("token", request.getResponseHeader("Authorization").replace("Bearer ", ""));
            localStorage.setItem("username", txtUsername.value);
            localStorage.setItem("projects", JSON.stringify(data));
            window.location = "portal.html";
        } else {
            alert("O backend respondeu de maneira inesperada.");
        }
      })
      .fail((data) => {
        if (data.hasOwnProperty("mensagem")) {
            alert(data.mensagem);
        } else {
            alert("Erro inesperado");
        }
      });
}

// Load events
checkLoggedInEvent();

// Event hooks
btnRegister.onclick = registerEvent;
btnLogin.onclick = loginEvent;