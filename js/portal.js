let authorizedProjects = localStorage.getItem("projects");

if (authorizedProjects === null) {
    window.location = "login.html";
}

authorizedProjects = JSON.parse(authorizedProjects);

authorizedProjects.forEach(project => {
    let html = `<a href="${project.url}">` +
                    '<div class="card p-5 m-2">' +
                        '<div class="card-body">' +
                            `<h1>${project.titulo}</h1>` +
                        '</div>' +
                    '</div>' +
               '</a>';
    let cardGroup = document.querySelector(".card-group");
    cardGroup.innerHTML += html;
});