// API URLs
const baseURL = "http://localhost:8090";
const registerEndpoint =  baseURL + "/projetos/registrar";

// DOM
const btnRegister = document.querySelector("#register");
const txtTitle = document.querySelector("#title");
const txtUrl = document.querySelector("#url");

// Events
function registerEvent() {

    let data = {
        "titulo": txtTitle.value,
        "url": txtUrl.value
    };

    $.ajax({
        url: registerEndpoint,
        type: "post",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        data: JSON.stringify(data),
        dataType: "json"
    })
        .done((data) => {
            if (data.hasOwnProperty("mensagem")) {
                alert(data.mensagem);
            }
            txtTitle.value = "";
            txtUrl.value = "";
        })
        .fail((data) => {
            if (data.hasOwnProperty("mensagem")) {
                alert(data.mensagem);
            } else {
                alert("Erro inesperado");
            }
        });
}

// Event hooks
btnRegister.onclick = registerEvent;