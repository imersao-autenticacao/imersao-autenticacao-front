// API URLs
const baseURL = "http://localhost:8090";
const registerEndpoint = baseURL + "/usuarios/registrar";
const getAllProjectsEndpoint = baseURL + "/projetos";

// DOM
const txtName = document.querySelector("#name");
const txtUsername = document.querySelector("#username");
const txtEmail = document.querySelector("#email");
const cbxProjects = document.querySelector("#projects");
const txtPassword = document.querySelector("#password");
const txtRepeatPassword = document.querySelector("#repeat-password");
const btnRegister = document.querySelector("#register");

// Events
function loadProjectsEvent() {
    $.ajax({
        url: getAllProjectsEndpoint,
        type: "get",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        dataType: "json"
    })
        .done((data) => {
            cbxProjects.innerHTML = "";
            data.forEach(project => {
                let projectOption = `<option value="${project.url}">${project.titulo}</option>`;
                cbxProjects.innerHTML += projectOption;
            });
        })
        .fail((data) => {
            if (data.hasOwnProperty("mensagem")) {
                alert(data.mensagem);
            } else {
                alert("Erro inesperado");
            }
        });
}

function registerEvent() {
    event.preventDefault();

    if (txtPassword.value !== txtRepeatPassword.value) {
        alert("As senhas não coincidem!");
        return;
    }

    let data = {
        "nome": txtName.value,
        "username": txtUsername.value,
        "email": txtEmail.value,
        "senha": txtPassword.value,
        "projetosAutorizados": getProjectSelectValues(cbxProjects)
    };
    
    $.ajax({
        url: registerEndpoint,
        type: "post",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        data: JSON.stringify(data),
        dataType: "json"
    })
        .done((data) => {
            if (data.hasOwnProperty("mensagem")) {
                alert(data.mensagem);
            }
            window.location = "login.html";
        })
        .fail((response) => {
            data = response.responseJSON;
            if (data.hasOwnProperty("mensagem")) {
                alert(data.mensagem);
            } else {
                alert("Erro inesperado");
            }
        });
}

function getProjectSelectValues(select) {
    var result = [];
    var options = select && select.options;
    var opt;

    for (var i = 0, iLen = options.length; i < iLen; i++) {
        opt = options[i];

        if (opt.selected) {            
            let item = {
                "titulo": opt.text,
                "url": opt.value
            };
            result.push(item);
        }
    }
    return result;
}

// Load events
loadProjectsEvent();

// Event hooks
btnRegister.onclick = registerEvent;
txtName.focus();

// jQuery plugins
$(".select2").select2({
    placeholder: "Selecione os projetos",
    width: "91%"
});