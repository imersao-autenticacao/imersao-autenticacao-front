# Front-end do projeto autenticação do curso de imersão mastertech

Este front-end depende do projeto de back-end: `imersao-autenticacao`.
Através deste front é possível:

- Registrar os Projetos (título, url da home);
- Registrar Usuários (nome, e-mail, username, senha, projetosAutorizados);

# Instruções

1. Cadastrar seu projeto através da página: `project.html`;
2. Vincular seu usuário com o projeto, através da página: `register.html`;
3. Realizar o login através da página: `login.html`;
4. Escolher seu projeto através da página: `portal.html`.

Este front guarda as seguintes informações na **localStorage** do browser, e estarão disponíveis no index do seu projeto:

`token` => Token de autenticação que deve ser enviado para requisições de endpoints seguros; <br />
`username` => Username do usuário logado; <br />
`projects` => Projetos que o usuário logado tem autorização.

Para acessar usar os trechos a seguir:

```
let token = localStorage.getItem("token");
let username = localStorage.getItem("username");
let projects = localStorage.getItem("projects");
```